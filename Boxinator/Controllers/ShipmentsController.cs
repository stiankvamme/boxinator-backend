﻿using AutoMapper;
using Boxinator.Mailtemplate;
using Boxinator.Models;
using Boxinator.Models.Domain;
using Boxinator.Models.DTO.Account;
using Boxinator.Models.DTO.Shipment;
using Boxinator.Services;
using Boxinator.Services.ShipmentService;
using Ganss.XSS;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Boxinator.Controllers
{
    [Route("api/shipments")]
    [ApiController]
    [EnableCors("AllowAllHeaders")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ShipmentsController : ControllerBase
    {
        // Add automapper via dependency injection
        private readonly IMapper _mapper;
        // Services work with domain objects, but controllers are still responsible for mapping
        private readonly IShipmentService _shipmentService;
        private readonly IAccountService _accountService;
        private readonly ICountryService _countryService;
        private readonly IConfiguration Configuration;
        
        public ShipmentsController(IMapper mapper, IShipmentService shipmentService, IAccountService accountService, ICountryService countryService, IConfiguration configuration)
        {
            _mapper = mapper;
            _shipmentService = shipmentService;
            _accountService = accountService;
            _countryService = countryService;
            Configuration = configuration;
        }

        /// <summary>
        /// Get all shipments in database
        /// </summary>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("get_all/{admin}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetShipments(int admin)
        {
            // Check if user is admin
            Account userAccount = await _accountService.GetAccountAsync(admin);
            if (userAccount.AccountType != AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllShipmentsAsync());
        }

        /// <summary>
        /// Get all non-cancelled and non-completed shipments
        /// </summary>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("active/{admin}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetActiveShipments(int admin)
        {
            // Check if user is admin
            Account userAccount = await _accountService.GetAccountAsync(admin);
            if (userAccount.AccountType != AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllActiveShipmentsAsync());
        }

        /// <summary>
        /// Get all completed shipments for a specific user in database
        /// </summary>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("completed/{id}/{user}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCompletedShipmentsById(int id, int user)
        {
            // If user is not admin, then id must be equal to user id, so regular users only can access their own shipments
            Account userAccount = await _accountService.GetAccountAsync(user);
            if (userAccount.AccountType == AccountType.REGISTERED_USER && userAccount.Id != id)
            {
                return Unauthorized();
            }

            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllCompletedShipmentsByIdAsync(id));
        }

        /// <summary>
        /// Get all completed shipments in database
        /// </summary>
        /// <param name="admin"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("completed/{admin}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCompletedShipments(int admin)
        {
            // Check if user is admin
            Account userAccount = await _accountService.GetAccountAsync(admin);
            if (userAccount.AccountType != AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllCompletedShipmentsAsync());
        }

        /// <summary>
        /// Get all non-completed shipments for a specific user in database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("non-completed/{id}/{user}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetNonCompletedShipments(int id, int user)
        {
            // If user is not admin, then id must be equal to user id, so regular users only can access their own shipments
            Account userAccount = await _accountService.GetAccountAsync(user);
            if (userAccount.AccountType == AccountType.REGISTERED_USER && userAccount.Id != id)
            {
                return Unauthorized();
            }

            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllNonCompletedShipmentsAsync(id));
        }

        /// <summary>
        /// Get all cancelled shipments for a specific user in database
        /// </summary>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("cancelled/{id}/{user}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCancelledShipmentsById(int id, int user)
        {
            // If user is not admin, then id must be equal to user id, so regular users only can access their own shipments
            Account userAccount = await _accountService.GetAccountAsync(user);
            if (userAccount.AccountType == AccountType.REGISTERED_USER && userAccount.Id != id)
            {
                return Unauthorized();
            }

            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllCancelledShipmentsByIdAsync(id));
        }

        /// <summary>
        /// Get all cancelled shipments in database
        /// </summary>
        /// <param name="admin"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("cancelled/{admin}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCancelledShipments(int admin)
        {
            // Check if user is admin
            Account userAccount = await _accountService.GetAccountAsync(admin);
            if (userAccount.AccountType != AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllCancelledShipmentsAsync());
        }

        /// <summary>
        /// Get a specific shipment by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("{id}/{user}")]
        public async Task<ActionResult<ShipmentReadDTO>> GetShipment(int id, int user)
        {
            Shipment shipment = await _shipmentService.GetSpecificShipmentAsync(id);

            if (shipment == null)
            {
                return NotFound();
            }

            // If user is not admin, then account ID must be equal to user ID, so regular users only can access their own shipments
            Account userAccount = await _accountService.GetAccountAsync(user);
            if (userAccount.AccountType == AccountType.REGISTERED_USER && userAccount.Id != shipment.AccountId)
            {
                return Unauthorized();
            }

            return _mapper.Map<ShipmentReadDTO>(shipment);
        }

        /// <summary>
        /// Get all shipments for a specific customer by account ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("customer/{id}/{user}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCustomerShipments(int id, int user)
        {
            // If user is not admin, then id must be equal to user id, so regular users only can access their own shipments
            Account userAccount = await _accountService.GetAccountAsync(user);
            if (userAccount.AccountType == AccountType.REGISTERED_USER && userAccount.Id != id)
            {
                return Unauthorized();
            }

            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<ShipmentReadDTO>>(await _shipmentService.GetAllShipmentsByCustomer(id));
        }

        /// <summary>
        /// Create new shipment, and send mail with reciept
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<ActionResult<ShipmentReadDTO>> PostShipment(ShipmentCreateDTO shipment)
        {
            // Check if receiver name is empty
            if (shipment.ReceiverName == "")
            {
                return BadRequest("Receiver name cannot be empty");
            }

            // Check that status is CREATED
            if (shipment.Status != StatusType.CREATED)
            {
                return BadRequest("Shipment status should be 0 (CREATED)");
            }

            // Check if shipment weight is valid
            int[] weights = { 1, 2, 5, 8 };
            if (!weights.Contains(shipment.Weight_kg))
            {
                return BadRequest("Shipment weight is not a valid weight");
            }

            // Check if destination country exists
            if (!_countryService.CountryExists(shipment.DestinationCountry))
            {
                return BadRequest("Destination country of shipment does not exist");
            }

            // Check that shipment price is correct
            Country destinationCountry = await _countryService.GetCountryAsync(shipment.DestinationCountry);
            double multiplier = destinationCountry.CountryMultiplier;
            int weight = shipment.Weight_kg;
            double price = multiplier * weight + 200;

            if (shipment.Price != price)
            {
                return BadRequest("The shipping price is not correct");
            }

            Shipment domainShipment = _mapper.Map<Shipment>(shipment);
            int accountId = (int)domainShipment.AccountId;

            if (_accountService.AccountExists(accountId))
            {
                // sanitize input
                HtmlSanitizer sanitize = new HtmlSanitizer();
                domainShipment.ReceiverName = sanitize.Sanitize(domainShipment.ReceiverName);
                domainShipment.BoxColour = sanitize.Sanitize(domainShipment.BoxColour);
                domainShipment.Date = sanitize.Sanitize(domainShipment.Date);

                domainShipment = await _shipmentService.AddShipmentAsync(domainShipment);
                Account user = await _accountService.GetAccountAsync(accountId);
                await CreateAndSendMailAsync(user.Email, domainShipment);
            }

            return _mapper.Map<ShipmentReadDTO>(domainShipment);

        }
        /// <summary>
        /// Creates a mail, and a mail template from html and adds values to shippment data, by reading html file to string, then replacing in that string 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="shippment"></param>
        /// <returns></returns>
        public async Task CreateAndSendMailAsync(string email, Shipment shippment)
        {
            //bind object model from configuration
            MyConfig conf = Configuration.GetSection(MyConfig.MailValues).Get<MyConfig>();
            var apiKey =conf.MailKey.ToString();//MailKey from appsettings.json;

            // var plainTextContent = "Your shipment:  " + ",  Date:  " + shippment.Date + ",     Colour:   " + shippment.BoxColour + ",    Price:   " + shippment.Price + ",    Destination:    " + shippment.DestinationCountry + ",   Reciver-name:  " + shippment.ReceiverName;

            //makes html file string 
            // comment in the line below to run locally
            string FilePath = "Mailtemplate\\reciept.html";
            //string FilePath = "Mailtemplate/reciept.html";
            StreamReader str = new StreamReader(FilePath);
            string MailText = str.ReadToEnd();
            str.Close();

            Country shipmentCountry = await _countryService.GetCountryAsync(shippment.DestinationCountry);

            //sets parameters in html string
            string mailSend = MailText.Replace("%%recivernametoreplace", shippment.ReceiverName);
            mailSend = mailSend.Replace("%%pricetoreplace", shippment.Price.ToString());
            mailSend = mailSend.Replace("%%destinationtoreplace", shipmentCountry.CountryName);
            mailSend = mailSend.Replace("%%weighttoreplace", shippment.Weight_kg.ToString());
            mailSend = mailSend.Replace("%%colortoreplace", shippment.BoxColour);
            mailSend = mailSend.Replace("%%datetoreplace", shippment.Date.ToString());

            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.Subject = "Reciept Boxinator";
            message.To.Add(email);
            message.Body = mailSend;
            message.From = new MailAddress("benjamin.andersen@no.experis.com", "Boxinator Creators");

            SmtpClient client2 = new SmtpClient("smtp.sendgrid.net");
            client2.UseDefaultCredentials = false;
            client2.Credentials = new NetworkCredential("apikey", apiKey);
            client2.DeliveryMethod = SmtpDeliveryMethod.Network;
            client2.Port = 587;
            client2.Timeout = 99999;
            client2.EnableSsl = true;

            await client2.SendMailAsync(message);
        }

        /// <summary>
        /// Update shipment by ID, only accessible for admins
        /// </summary>
        /// <param name="id">Shipment id</param>
        /// <param name="shipment"></param>
        /// <returns>NoContent</returns>
        [GoogleAuthorize]
        [HttpPut("admin_update/{id}/{admin}")]
        public async Task<IActionResult> PutShipmentAdmin(int id, ShipmentEditDTO shipment, int admin)
        {
            // Check if receiver name is empty
            if (shipment.ReceiverName == "")
            {
                return BadRequest("Receiver name cannot be empty");
            }

            // Check is status is valid
            if (!Enum.IsDefined(typeof(StatusType), shipment.Status))
            {
                return BadRequest("Shipment status is not valid");
            }

            // Check if shipment weight is valid
            int[] weights = { 1, 2, 5, 8 };
            if (!weights.Contains(shipment.Weight_kg))
            {
                return BadRequest("Shipment weight is not a valid weight");
            }

            // Check if destination country exists
            if (!_countryService.CountryExists(shipment.DestinationCountry))
            {
                return BadRequest("Destination country of shipment does not exist");
            }

            // Check that shipment price is correct
            Country destinationCountry = await _countryService.GetCountryAsync(shipment.DestinationCountry);
            double multiplier = destinationCountry.CountryMultiplier;
            int weight = shipment.Weight_kg;
            double price = multiplier * weight + 200;

            if (shipment.Price != price)
            {
                return BadRequest("The shiping price is not correct");
            }

            // Check if user is admin
            Account userAccount = await _accountService.GetAccountAsync(admin);
            if (userAccount.AccountType != AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            // Throws BadRequest response if IDs don not match
            if (id != shipment.Id)
            {
                return BadRequest();
            }

            // Throws NotFound response if not found
            if (!_shipmentService.ShipmentExists(id))
            {
                return NotFound();
            }

            // Check if shipment accountId and admin userId are the same
            if (shipment.AccountId == userAccount.Id)
            {
                return Unauthorized();
            }

            Shipment domainShipment = _mapper.Map<Shipment>(shipment);

            // sanitize input
            HtmlSanitizer sanitize = new HtmlSanitizer();
            domainShipment.ReceiverName = sanitize.Sanitize(domainShipment.ReceiverName);
            domainShipment.BoxColour = sanitize.Sanitize(domainShipment.BoxColour);
            domainShipment.Date = sanitize.Sanitize(domainShipment.Date);

            await _shipmentService.UpdateShipmentAdminAsync(domainShipment);

            return NoContent();
        }

        /// <summary>
        /// Change shipment status to CANCELLED
        /// Check if userId same as accountId on shipment
        /// Check if status is valid (CANCELLED)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="shipmentId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpPut("user_update/cancel/{user}")]
        public async Task<IActionResult> PutShipmentUser(ShipmentReadDTO ship, int user)
        {
            // Throws NotFound response if not found
            if (!_shipmentService.ShipmentExists(ship.Id))
            {
                return NotFound();
            }

            // get shipment by id
            Shipment shipment = await _shipmentService.GetSpecificShipmentAsync(ship.Id);

            // check shipment user id == userId
            if (shipment.AccountId == user)
            {
                shipment.Status = StatusType.CANCELLED;

                await _shipmentService.UpdateShipmentUserAsync(shipment);
                return NoContent();
            }
            return Unauthorized();
        }

        /// <summary>
        /// Delete a shipment by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpDelete("{id}/{admin}")]
        public async Task<IActionResult> DeleteShipment(int id, int admin)
        {
            // Check if user is admin
            Account userAccount = await _accountService.GetAccountAsync(admin);
            if (userAccount.AccountType != AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            // Throws NotFound response if not found
            if (!_shipmentService.ShipmentExists(id))
            {
                return NotFound();
            }

            await _shipmentService.DeleteShipmentAsync(id);
            return NoContent();
        }
    }
}
