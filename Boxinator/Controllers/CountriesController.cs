﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTO.Country;
using Boxinator.Services;
using Ganss.XSS;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace Boxinator.Controllers
{
    [Route("api/countries")]
    [ApiController]
    [EnableCors("AllowAllHeaders")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CountriesController : ControllerBase
    {
        // Add automapper via dependecy injection
        private readonly IMapper _mapper;

        // Services work with domain objects, but controllers are still responsible for mapping
        private readonly ICountryService _countryService;
        private readonly IAccountService _accountService;

        public CountriesController(IMapper mapper, ICountryService countryService, IAccountService accountService)
        {
            _mapper = mapper;
            _countryService = countryService;
            _accountService = accountService;
        }

        /// <summary>
        /// Get all countries
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CountryReadDTO>>> GetCountries()
        {
            return _mapper.Map<List<CountryReadDTO>>(await _countryService.GetAllCountriesAsync());
        }

        /// <summary>
        /// Add a new country with all relevant information
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpPost("add/{adminUser}")]
        public async Task<ActionResult<CountryReadDTO>> PostCountry(CountryCreateDTO country, int adminUser)
        {
            // Get user account for admin accessing this method, and check if AccountType is admin
            Account adminAccount = await _accountService.GetAccountAsync(adminUser);
            if (adminAccount.AccountType != Models.AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            if (_countryService.CountryNameExists(country.CountryName))
            {
                return Conflict();
            }

            // Check if country name is empty string
            if (country.CountryName == "")
            {
                return BadRequest("Country name cannot be empty");
            }

            // Check if country multiplier is 0 or less
            if (country.CountryMultiplier <= 0)
            {
                return BadRequest("Country multiplier cannot be 0 or less");
            }

            // Sanitizing input string before adding to avoid XSS attacks
            Country domainCountry = _mapper.Map<Country>(country);
            HtmlSanitizer sanitizer = new HtmlSanitizer();
            domainCountry.CountryName = sanitizer.Sanitize(domainCountry.CountryName);
            domainCountry = await _countryService.AddCountryAsync(domainCountry);

            return _mapper.Map<CountryReadDTO>(domainCountry);
        }

        /// <summary>
        /// Update a specific country by ID
        /// (must pass a full country object and ID in route)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpPut("update/{id}/{adminUser}")]
        public async Task<IActionResult> PutCountry(int id, CountryEditDTO country, int adminUser)
        {
            // Get user account for admin accessing this method, and check if AccountType is admin
            Account adminAccount = await _accountService.GetAccountAsync(adminUser);
            if (adminAccount.AccountType != Models.AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            // Throws Bad Request response if IDs do not match
            if (id != country.Id)
            {
                return BadRequest();
            }

            // Throws Not Found response if country is not found
            if (!_countryService.CountryExists(id))
            {
                return NotFound();
            }

            // Check if country multiplier is 0 or less
            if (country.CountryMultiplier <= 0)
            {
                return BadRequest("Country multiplier cannot be 0 or less");
            }

            // Sanitizing input string before updating to avoid XSS attacks
            Country domainCountry = _mapper.Map<Country>(country);
            HtmlSanitizer sanitizer = new HtmlSanitizer();
            domainCountry.CountryName = sanitizer.Sanitize(domainCountry.CountryName);
            await _countryService.UpdateCountryAsync(domainCountry);

            return NoContent();
        }
    }
}
