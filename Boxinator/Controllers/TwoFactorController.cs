﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTO.TwoFactor;
using Boxinator.Services;
using Boxinator.Services.TwoFactorService;
using Boxinator.VerificationCode;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Boxinator.Controllers
{
    [Route("api/twofactor")]
    [ApiController]
    [EnableCors("AllowAllHeaders")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class TwoFactorController : ControllerBase
    {
        private readonly IMapper _mapper;

        private ITwoFactorService _twoFactorService;

        private readonly IConfiguration _configuration;

        private readonly IAccountService _accountService;
        public TwoFactorController(IMapper mapper, ITwoFactorService twoFactorService, IAccountService accountService, IConfiguration configuration)
        {
            _mapper = mapper;
            _twoFactorService = twoFactorService;
            _accountService = accountService;
            _configuration = configuration;
        }

        /// <summary>
        /// Get a TwoFactor code from the database by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<TwoFactor>> GetTwoFactor(int id)
        {
            TwoFactor twofactor = await _twoFactorService.GetTwoFactorAsync(id);

            if (twofactor == null)
            {
                return NotFound();
            }

            return _mapper.Map<TwoFactor>(twofactor);
        }

        /// <summary>
        /// Check if the verification code that the user has entered is the same as in database, and within six minutes of when it was registered.
        /// Deletes the verification code from the database after validation if correct.
        /// </summary>
        /// <param name="twoFactor"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpPost("two_factor_verification")]
        public async Task<ActionResult<Boolean>> GetTwoFactorVerification(TwoFactor twoFactor)
        {
            TwoFactor domainTwoFactor = await _twoFactorService.GetTwoFactorAsync(twoFactor.UserId);

            //TwoFactor twoFactor1 = _mapper.Map<TwoFactor>(twoFactor);
            twoFactor.TimeStamp = DateTime.UtcNow;

            if (twoFactor.SixDigitCode == domainTwoFactor.SixDigitCode && twoFactor.TimeStamp <= domainTwoFactor.TimeStamp)
            {
                await _twoFactorService.DeleteTwoFactorAsync(domainTwoFactor.Id);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Add verification code to database (with timestamp)
        /// </summary>
        /// <param name="twoFactor"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpPost("two_factor_code")]
        public async Task<ActionResult<TwoFactor>> PostTwoFactor(TwoFactor twoFactor)
        {
            TwoFactor domainTwoFactor = _mapper.Map<TwoFactor>(twoFactor);
            domainTwoFactor = await _twoFactorService.AddTwoFactorAsync(domainTwoFactor);

            return CreatedAtAction("GetTwoFactor",
                new { id = domainTwoFactor.Id },
                _mapper.Map<TwoFactor>(domainTwoFactor));
        }


        /// <summary>
        /// Delete verification code by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpDelete]
        public async Task<IActionResult> DeleteTwoFactor(int id)
        {
            TwoFactor twoFactor = await _twoFactorService.GetTwoFactorAsync(id);
            await _twoFactorService.DeleteTwoFactorAsync(twoFactor.Id);
            return NoContent();
        }

        /// <summary>
        /// Get new TwoFactor code
        /// Pressed if user need a new code after unsuccessfully trying to validate with the first.
        /// Get's 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("new_two_factor_code/{userId}")]
        public async Task<IActionResult> GetNewTwoFactorCode(int userId)
        {
            if (userId > 0)
            {
                //get and delete old code
                if (_twoFactorService.TwoFactorCodeExists(userId))
                {
                    TwoFactor domainTwoFactorDelete = await _twoFactorService.GetTwoFactorAsync(userId);
                    await _twoFactorService.DeleteTwoFactorAsync(domainTwoFactorDelete.Id);
                }

                //generate new verification code
                Random6DigitCode random6DigitCode = new Random6DigitCode();
                string code = random6DigitCode.RandomNum();

                //create new Twofactor object and save to DB
                TwoFactor twoFactor = new TwoFactor();
                twoFactor.SixDigitCode = Int32.Parse(code);
                twoFactor.UserId = userId;
                await _twoFactorService.AddTwoFactorAsync(twoFactor);

                //get user and send mail
                Account currentUser = await _accountService.GetAccountAsync(userId);
                SendVerificationCodeMail svcm = new SendVerificationCodeMail(_configuration);
                await svcm.SendCodeMail(code, currentUser.Email, currentUser.FirstName, currentUser.LastName);

                return Ok();
            }
            else
            {
                // if user ID is null
                return BadRequest();
            }
        }
    }
}
