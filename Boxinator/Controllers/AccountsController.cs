﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Boxinator.Services;
using Boxinator.Models.DTO.Account;
using Boxinator.Models.Domain;
using Microsoft.AspNetCore.Cors;
using Boxinator.VerificationCode;
using Microsoft.Extensions.Configuration;
using Boxinator.Mailtemplate;
using Boxinator.Services.TwoFactorService;
using Ganss.XSS;
using Boxinator.Models;

namespace Boxinator.Controllers
{

    [Route("api/accounts")]
    [ApiController]
    [EnableCors("AllowAllHeaders")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class AccountsController : ControllerBase
    {
        // Add automapper via dependency injection
        private readonly IMapper _mapper;

        // Services work with domain objects, but controllers are still responsible for mapping
        private readonly IAccountService _accountService;

        private readonly IConfiguration _configuration;

        private ITwoFactorService _twoFactorService;
        public AccountsController(IMapper mapper, IAccountService accountService, IConfiguration configuration, ITwoFactorService twoFactorService)
        {
            _mapper = mapper;
            _accountService = accountService;
            _configuration = configuration;
            _twoFactorService = twoFactorService;
        }

        /// <summary>
        /// Get all acounts in database
        /// </summary>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpGet("all_accounts/{admin}")]
        public async Task<ActionResult<IEnumerable<AccountReadDTO>>> GetAccounts(int admin)
        {
            // Check if user is admin
            Account userAccount = await _accountService.GetAccountAsync(admin);
            if (userAccount.AccountType != AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            return _mapper.Map<List<AccountReadDTO>>(await _accountService.GetAllAccountsAsync());
        }

        /// <summary>
        /// Get a specific account by ID
        /// </summary>
        /// <param name="id">Account ID</param>
        /// <returns>Account</returns>
        [GoogleAuthorize]
        [HttpGet("{id}/{user}")]
        public async Task<ActionResult<AccountReadDTO>> GetAccount(int id, int user)
        {
            // Check that if user is registered user, then ID must be same as user ID - returns unauthorized if not
            Account currentUser = await _accountService.GetAccountAsync(user);
            if (currentUser.AccountType == Models.AccountType.REGISTERED_USER && currentUser.Id != id)
            {
                return Unauthorized();
            }

            Account account = await _accountService.GetAccountAsync(id);

            // Throws NotFound response if not found
            if (account == null)
            {
                return NotFound();
            }

            return _mapper.Map<AccountReadDTO>(account);
        }

        /// <summary>
        /// Update a specific account by ID
        /// Note: account type can not be modified
        /// </summary>
        /// <param name="id">Account ID</param>
        /// <param name="account">Full account object</param>
        /// <returns>NoContent</returns>
        [GoogleAuthorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccount(int id, AccountEditDTO account)
        {
            // Throws BadRequest response if IDs don not match
            if (id != account.Id)
            {
                return BadRequest();
            }

            // Throws NotFound response if not found
            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            // Check if required inputs are empty
            if (account.FirstName == "")
            {
                return BadRequest("First name cannot be empty");
            }

            if (account.LastName == "")
            {
                return BadRequest("Last name cannot be empty");
            }

            if (account.Email == "")
            {
                return BadRequest("Email cannot be empty");
            }
            //sanitize strings in account
            Account domainAccount = _mapper.Map<Account>(account);
            HtmlSanitizer sanitize = new HtmlSanitizer();
            domainAccount.FirstName = sanitize.Sanitize(domainAccount.FirstName);
            domainAccount.LastName = sanitize.Sanitize(domainAccount.LastName);
            domainAccount.PostalCode = sanitize.Sanitize(domainAccount.PostalCode);
            domainAccount.Country = sanitize.Sanitize(domainAccount.Country);
            domainAccount.DateOfBirth = sanitize.Sanitize(domainAccount.DateOfBirth);
            domainAccount.Email = sanitize.Sanitize(domainAccount.Email);
            domainAccount.ContactNumber = sanitize.Sanitize(domainAccount.ContactNumber);

            await _accountService.UpdateAccountAsync(domainAccount);

            return NoContent();
        }

        /// <summary>
        /// Update a specific user's AccountType to become Administrator by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="adminUser"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpPut("make_admin/{id}/{adminUser}")]
        public async Task<IActionResult> PutAdmin(int id, int adminUser)
        {
            // Get user account for admin accessing this method, and check if AccountType is admin
            Account adminAccount = await _accountService.GetAccountAsync(adminUser);
            if (adminAccount.AccountType != Models.AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            // Throws NotFound response if not found
            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            // Get user account from the ID that is going to be made admin, and change AccountType to admin
            Account account = await _accountService.GetAccountAsync(id);
            account.AccountType = Models.AccountType.ADMINISTRATOR;
            await _accountService.UpdateAdminAccountAsync(account);

            return NoContent();
        }

        /// <summary>
        /// Update a specific user's AccountType, to change from Administrator to Registered User, by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="adminUser"></param>
        /// <returns></returns>
        [GoogleAuthorize]
        [HttpPut("remove_admin/{id}/{adminUser}")]
        public async Task<IActionResult> PutAdminAway(int id, int adminUser)
        {
            // Get user account for admin accessing this method, and check if AccountType is admin
            Account adminAccount = await _accountService.GetAccountAsync(adminUser);
            if (adminAccount.AccountType != Models.AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            // Throws NotFound response if not found
            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            // Get user account from the ID that is going to have admin rights removed, and change AccountType to admin
            Account account = await _accountService.GetAccountAsync(id);
            account.AccountType = Models.AccountType.REGISTERED_USER;
            await _accountService.UpdateAdminAccountAsync(account);

            return NoContent();
        }

        /// <summary> 
        /// Add a new account to the database
        /// </summary>
        /// <param name="account">Full account object</param>
        /// <returns>Account</returns>
        [GoogleAuthorize]
        [HttpPost("register_user")]
        public async Task<ActionResult<AccountReadDTO>> PostAccount(AccountCreateDTO account)
        {
            // check for duplicate email
            if (_accountService.AccountEmailExists(account.Email))
            {
                return Conflict("E-mail already exists");
            }

            // Check if required inputs are empty
            if (account.FirstName == "")
            {
                return BadRequest("First name cannot be empty");
            }

            if (account.LastName == "")
            {
                return BadRequest("Last name cannot be empty");
            }

            if (account.Email == "")
            {
                return BadRequest("Email cannot be empty");
            }

            Account domainAccount = _mapper.Map<Account>(account);
            domainAccount.AccountType = Models.AccountType.REGISTERED_USER;

            //sanitize input strings
            HtmlSanitizer sanitize = new HtmlSanitizer();
            domainAccount.FirstName = sanitize.Sanitize(domainAccount.FirstName);
            domainAccount.LastName = sanitize.Sanitize(domainAccount.LastName);
            domainAccount.PostalCode = sanitize.Sanitize(domainAccount.PostalCode);
            domainAccount.Country = sanitize.Sanitize(domainAccount.Country);
            domainAccount.DateOfBirth = sanitize.Sanitize(domainAccount.DateOfBirth);
            domainAccount.Email = sanitize.Sanitize(domainAccount.Email);
            domainAccount.ContactNumber = sanitize.Sanitize(domainAccount.ContactNumber);

            domainAccount = await _accountService.AddAccountAsync(domainAccount);
            AccountReadDTO t = _mapper.Map<AccountReadDTO>(domainAccount);

            //create verificaiton code, store it and send mail
            Random6DigitCode random6DigitCode = new Random6DigitCode();
            string code = random6DigitCode.RandomNum();
            SendVerificationCodeMail svcm = new SendVerificationCodeMail(_configuration);
            await svcm.SendCodeMail(code, t.Email, t.FirstName, t.LastName);

            TwoFactor twoFactor = new TwoFactor();
            twoFactor.SixDigitCode = Int32.Parse(code);
            twoFactor.UserId = t.Id;
            TwoFactor domainTwoFactor = await _twoFactorService.AddTwoFactorAsync(twoFactor);
            return t;
        }

        /// <summary>
        /// Add a new guest user to the database
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("guest")]
        public async Task<ActionResult<AccountReadDTO>> PostGuestAccount(AccountCreateDTO email)
        {
            // check for duplicate mail
            if (_accountService.AccountEmailExists(email.Email))
            {
                Account guest = await _accountService.GetAccountFromEmailAsync(email.Email);
                return _mapper.Map<AccountReadDTO>(guest);
            }
            Account domainAccount = _mapper.Map<Account>(email);
            domainAccount.AccountType = Models.AccountType.GUEST;
            domainAccount.FirstName = "";
            domainAccount.LastName = "";
            domainAccount.DateOfBirth = "";
            domainAccount.ContactNumber = "";
            domainAccount.Country = "";
            domainAccount.PostalCode = "";
            domainAccount.Email = email.Email;

            //sanitize inputStrings
            HtmlSanitizer sanitize = new HtmlSanitizer();
            domainAccount.Email = sanitize.Sanitize(domainAccount.Email);


            domainAccount = await _accountService.AddAccountAsync(domainAccount);

            return _mapper.Map<AccountReadDTO>(domainAccount);
        }
        /// <summary>
        /// Authorize user with Google
        /// Checks if user already exists or is a new user
        /// </summary>
        /// <param name="account">Full account object</param>
        /// <returns>Account</returns>
        [GoogleAuthorize]
        [HttpPost("google_auth")]
        public async Task<ActionResult<AccountReadDTO>> PostGoogleAccount(AccountReadDTO user)
        {
            //sanitize input
            HtmlSanitizer sanitize = new HtmlSanitizer();
            user.Email = sanitize.Sanitize(user.Email);

            // check for duplicate email
            if (_accountService.AccountEmailExists(user.Email))
            {
                Account account = await _accountService.GetAccountFromEmailAsync(user.Email);

                Random6DigitCode random6DigitCode = new Random6DigitCode();
                string code = random6DigitCode.RandomNum();
                SendVerificationCodeMail svcm = new SendVerificationCodeMail(_configuration);
                await svcm.SendCodeMail(code, user.Email, account.FirstName, account.LastName);

                //get and delete old code
                if (_twoFactorService.TwoFactorCodeExists(account.Id))
                {
                    TwoFactor domainTwoFactorDelete = await _twoFactorService.GetTwoFactorAsync(account.Id);
                    await _twoFactorService.DeleteTwoFactorAsync(domainTwoFactorDelete.Id);
                }

                TwoFactor twoFactor = new TwoFactor();
                twoFactor.SixDigitCode = Int32.Parse(code);
                twoFactor.UserId = account.Id;
                TwoFactor domainTwoFactor = await _twoFactorService.AddTwoFactorAsync(twoFactor);

                return _mapper.Map<AccountReadDTO>(account);
            }

            return NoContent();
        }

        /// <summary>
        /// Delete an account by specific ID
        /// </summary>
        /// <param name="id">Account ID</param>
        /// <returns>NoContent</returns>
        [GoogleAuthorize]
        [HttpDelete("{id}/{adminUser}")]
        public async Task<IActionResult> DeleteAccount(int id, int adminUser)
        {
            // Get user account for admin accessing this method, and check if AccountType is admin
            Account adminAccount = await _accountService.GetAccountAsync(adminUser);
            if (adminAccount.AccountType != Models.AccountType.ADMINISTRATOR)
            {
                return Unauthorized();
            }

            // Throws NotFound response if not found
            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            // Removes relationship between shipment(FK) and account, so shipments won't be affected
            Account account = await _accountService.GetAccountAsync(id);
            foreach (var shipment in account.Shipments)
            {
                shipment.Account = null;
            }
            await _accountService.DeleteAccountAsync(id);
            return NoContent();
        }
    }
}
