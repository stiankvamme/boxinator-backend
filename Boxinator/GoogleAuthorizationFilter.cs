﻿using Google.Apis.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Boxinator
{
    public class GoogleAuthorizeAttribute : TypeFilterAttribute
    {
        public GoogleAuthorizeAttribute() : base(typeof(GoogleAuthorizeFilter)) { }
    }

    public class GoogleAuthorizeFilter : IAuthorizationFilter
    {
        public GoogleAuthorizeFilter() { }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // Verify authorization header exists
            try
            {
                var headers = context.HttpContext.Request.Headers;
                if (!headers.ContainsKey("Authorization"))
                {
                    context.Result = new ForbidResult();
                }
                if (!headers.TryGetValue("Authorization", out var Authorization))
                {
                    context.Result = new ForbidResult();
                }
                var authHeader = Authorization.First();

                // Verify authorization header starts with bearer and has a token
                if(!authHeader.StartsWith("Bearer ") && authHeader.Length > 7)
                {
                    context.Result = new ForbidResult();
                }

                // Grab the token and verify through Google. If verification fails, an excecution will be thown.
                var token = authHeader.Remove(0, 7);
                var validated = GoogleJsonWebSignature.ValidateAsync(token, new GoogleJsonWebSignature.ValidationSettings()).Result;
            }
            catch (Exception)
            {
                context.Result = new ForbidResult();
            }
        }
    }
}
