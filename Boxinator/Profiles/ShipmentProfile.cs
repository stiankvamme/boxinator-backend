﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTO.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Profiles
{
    public class ShipmentProfile : Profile
    {
        public ShipmentProfile()
        {
            // Shipment <-> ShipmentReadDTO
            CreateMap<Shipment, ShipmentReadDTO>()
                // Turning related accounts into arrays
                .ForMember(adto => adto.AccountId, opt => opt
                .MapFrom(s => s.AccountId))
                .ReverseMap();

            // ShipmentCreateDTO <-> Shipment
            CreateMap<ShipmentCreateDTO, Shipment>().ReverseMap();

            // ShipmentEditDTO <-> Shipment
            CreateMap<ShipmentEditDTO, Shipment>().ReverseMap();
        }
    }
}
