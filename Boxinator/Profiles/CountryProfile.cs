﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTO.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Profiles
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            // Country <-> CountryReadDTO
            CreateMap<Country, CountryReadDTO>().ReverseMap();

            // CountryCreateDTO <-> Country
            CreateMap<CountryCreateDTO, Country>().ReverseMap();

            // CountryEditDTO <-> Country
            CreateMap<CountryEditDTO, Country>().ReverseMap();
        }
    }
}
