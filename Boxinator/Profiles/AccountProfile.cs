﻿using AutoMapper;
using Boxinator.Models.Domain;
using Boxinator.Models.DTO.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Profiles
{
    public class AccountProfile : Profile
    {

        public AccountProfile()
        {
            // Account <-> AccountReadDTO
            CreateMap<Account, AccountReadDTO>()
                // turning related shipments into int arrays
                .ForMember(adto => adto.Shipments, opt => opt
                .MapFrom(a => a.Shipments.Select(a => a.Id).ToArray()))
                .ReverseMap();

            // Account <-> AccountCreateDTO
            CreateMap<Account, AccountCreateDTO>().ReverseMap();

            // Account <-> AccountEditDTO
            CreateMap<Account, AccountEditDTO>().ReverseMap();
        }
    }
}
