﻿using Boxinator.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models
{
    public class BoxinatorDbContext : DbContext
    {
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<TwoFactor> TwoFactor { get; set; }

        public BoxinatorDbContext([NotNullAttribute] DbContextOptions options) 
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Account>().HasData(new Account
            {
                Id = 1,
                FirstName = "Benjamin",
                LastName = "Andersen",
                Email = "benjaminandersen94@gmail.com",
                Country = "France",
                AccountType = AccountType.ADMINISTRATOR
            });

            modelBuilder.Entity<Account>().HasData(new Account
            {
                Id = 2,
                FirstName = "Embla",
                LastName = "Øye",
                Email = "emblaoye@gmail.com",
                Country = "Norway",
                AccountType = AccountType.ADMINISTRATOR
            });

            modelBuilder.Entity<Account>().HasData(new Account
            {
                Id = 3,
                FirstName = "Stian",
                LastName = "Kvamme",
                Email = "stian.kvamme98@gmail.com",
                Country = "Russia",
                AccountType = AccountType.ADMINISTRATOR
            });

            modelBuilder.Entity<Shipment>().HasData(new Shipment
            {
                Id = 1,
                ReceiverName = "Embla Øye",
                Status = StatusType.INTRANSIT,
                DestinationCountry = 1,
                Weight_kg = 5,
                BoxColour = "rgba(250,128,114,1)",
                Price = 205,
                AccountId = 2
            });

            modelBuilder.Entity<Shipment>().HasData(new Shipment
            {
                Id = 2,
                ReceiverName = "Embla Øye",
                Status = StatusType.COMPLETED,
                DestinationCountry = 1,
                Weight_kg = 8,
                BoxColour = "rgba(250,128,114,1)",
                Price = 208,
                AccountId = 2
            });

            modelBuilder.Entity<Shipment>().HasData(new Shipment
            {
                Id = 3,
                ReceiverName = "Embla Øye",
                Status = StatusType.CREATED,
                DestinationCountry = 1,
                Weight_kg = 2,
                BoxColour = "rgba(250,128,114,1)",
                Price = 202,
                AccountId = 2
            });

            modelBuilder.Entity<Shipment>().HasData(new Shipment
            {
                Id = 4,
                ReceiverName = "Embla Øye",
                Status = StatusType.RECEIVED,
                DestinationCountry = 1,
                Weight_kg = 5,
                BoxColour = "rgba(250,128,114,1)",
                Price = 205,
                AccountId = 2
            });

            modelBuilder.Entity<Shipment>().HasData(new Shipment
            {
                Id = 5,
                ReceiverName = "Stian Kvamme",
                Status = StatusType.CANCELLED,
                DestinationCountry = 14,
                Weight_kg = 2,
                BoxColour = "rgba(2,255,0,0.5)",
                Price = 214,
                AccountId = 3
            });

            modelBuilder.Entity<Shipment>().HasData(new Shipment
            {
                Id = 6,
                ReceiverName = "Benjamin Andersen",
                Status = StatusType.COMPLETED,
                DestinationCountry = 5,
                Weight_kg = 8,
                BoxColour = "rgba(250,128,114,1)",
                Price = 248,
                AccountId = 1
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 1,
                CountryName = "Norway",
                CountryMultiplier = 1
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 2,
                CountryName = "Sweden",
                CountryMultiplier = 1
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 3,
                CountryName = "Denmark",
                CountryMultiplier = 1
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 4,
                CountryName = "Germany",
                CountryMultiplier = 5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 5,
                CountryName = "France",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 6,
                CountryName = "Spain",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 7,
                CountryName = "Belgium",
                CountryMultiplier = 5.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 8,
                CountryName = "Netherlands",
                CountryMultiplier = 5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 9,
                CountryName = "Poland",
                CountryMultiplier = 5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 10,
                CountryName = "United Kingdom",
                CountryMultiplier = 4
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 11,
                CountryName = "Ireland",
                CountryMultiplier = 4.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 12,
                CountryName = "Finland",
                CountryMultiplier = 2
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 13,
                CountryName = "Iceland",
                CountryMultiplier = 3
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 14,
                CountryName = "Russia",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 15,
                CountryName = "Italy",
                CountryMultiplier = 6.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 16,
                CountryName = "Czechia",
                CountryMultiplier = 5.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 17,
                CountryName = "Slovakia",
                CountryMultiplier = 5.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 18,
                CountryName = "Austria",
                CountryMultiplier = 5.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 19,
                CountryName = "Switzerland",
                CountryMultiplier = 5.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 20,
                CountryName = "Slovenia",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 21,
                CountryName = "Croatia",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 22,
                CountryName = "Hungary",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 23,
                CountryName = "Bosnia and Herzegovina",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 24,
                CountryName = "Serbia",
                CountryMultiplier = 6.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 25,
                CountryName = "Romania",
                CountryMultiplier = 6.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 26,
                CountryName = "Bulgaria",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 27,
                CountryName = "Greece",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 28,
                CountryName = "Turkey",
                CountryMultiplier = 7.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 29,
                CountryName = "Montenegro",
                CountryMultiplier = 6.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 30,
                CountryName = "Albania",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 31,
                CountryName = "North Macedonia",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 32,
                CountryName = "Kosovo",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 33,
                CountryName = "Moldova",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 34,
                CountryName = "Portugal",
                CountryMultiplier = 7
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 35,
                CountryName = "Ukraine",
                CountryMultiplier = 6.5
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 36,
                CountryName = "Belarus",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 37,
                CountryName = "Lithuania",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 38,
                CountryName = "Latvia",
                CountryMultiplier = 6
            });

            modelBuilder.Entity<Country>().HasData(new Country
            {
                Id = 39,
                CountryName = "Estonia",
                CountryMultiplier = 6
            });

        }
    }
}
