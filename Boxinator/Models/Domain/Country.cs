﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.Domain
{
    [Table("Country")]
    public class Country
    {
        // Primary Key
        public int Id { get; set; }
        public string CountryName { get; set; }
        public double CountryMultiplier { get; set; }
    }
}
