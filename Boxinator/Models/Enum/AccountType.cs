﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models
{
    public enum AccountType
    {
        GUEST,
        REGISTERED_USER,
        ADMINISTRATOR
    }
}
