﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.DTO.Account
{
    public class AccountCreateDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string ContactNumber { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public AccountType AccountType { get; set; }
    }
}
