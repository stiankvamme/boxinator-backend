﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.DTO.TwoFactor
{
    public class TwoFactorReadDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int SixDigitCode { get; set; }
    }
}
