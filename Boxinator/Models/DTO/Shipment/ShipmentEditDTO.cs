﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.DTO.Shipment
{
    public class ShipmentEditDTO
    {
        public int Id { get; set; }
        public string ReceiverName { get; set; }
        public StatusType Status { get; set; }
        public int DestinationCountry { get; set; }
        public int Weight_kg { get; set; }
        public string BoxColour { get; set; }
        public double Price { get; set; }
        //public string Date { get; set; }
        public int AccountId { get; set; }
    }
}
