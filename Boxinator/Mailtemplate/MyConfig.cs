﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Mailtemplate
{
    public class MyConfig
    {
        public const string MailValues = "MailValues";
        public string MailKey { get; set; }
        public string MailSender { get; set; }
        public string MailSenderName { get; set; }
    }
}
