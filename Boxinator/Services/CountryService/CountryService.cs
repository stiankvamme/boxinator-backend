﻿using Boxinator.Models;
using Boxinator.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services.CountryService
{
    public class CountryService : ICountryService
    {

        private readonly BoxinatorDbContext _context;

        public CountryService(BoxinatorDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Country>> GetAllCountriesAsync()
        {
            return await _context.Countries.ToListAsync();
        }

        public async Task<Country> GetCountryAsync(int id)
        {
            return await _context.Countries
                .Where(a => a.Id == id)
                .FirstAsync();
        }

        public async Task<Country> AddCountryAsync(Country country)
        {
            _context.Countries.Add(country);
            await _context.SaveChangesAsync();
            return country;
        }

        public async Task UpdateCountryAsync(Country country)
        {
            _context.Entry(country).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.Id == id);
        }

        public bool CountryNameExists(string name)
        {
            return _context.Countries.Any(e => e.CountryName == name);
        }
    }
}
