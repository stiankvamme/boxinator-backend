﻿using Boxinator.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services
{
    public interface ICountryService
    {
        public Task<IEnumerable<Country>> GetAllCountriesAsync();
        public Task<Country> GetCountryAsync(int id);
        public Task<Country> AddCountryAsync(Country country);
        public Task UpdateCountryAsync(Country country);
        public bool CountryExists(int id);
        public bool CountryNameExists(string name);
    }
}
