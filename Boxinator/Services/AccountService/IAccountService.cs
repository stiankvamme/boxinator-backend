﻿using Boxinator.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services
{
    public interface IAccountService
    {
        public Task<IEnumerable<Account>> GetAllAccountsAsync();
        public Task<Account> GetAccountAsync(int id);
        public Task<Account> GetAccountFromEmailAsync(string email);
        public Task UpdateAccountAsync(Account account);
        public Task UpdateAdminAccountAsync(Account account);
        public Task<Account> AddAccountAsync(Account account);
        public Task DeleteAccountAsync(int id);
        public bool AccountExists(int id);
        public bool AccountEmailExists(string email);

        //public Task<Account> AddGoogleAccountAsync(Account account);
    }
}
