﻿using Boxinator.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services.ShipmentService
{
    public interface IShipmentService
    {
        public Task<IEnumerable<Shipment>> GetAllShipmentsAsync();
        public Task<IEnumerable<Shipment>> GetAllActiveShipmentsAsync();
        public Task<IEnumerable<Shipment>> GetAllCompletedShipmentsByIdAsync(int id);
        public Task<IEnumerable<Shipment>> GetAllCompletedShipmentsAsync();
        public Task<IEnumerable<Shipment>> GetAllNonCompletedShipmentsAsync(int id);
        public Task<IEnumerable<Shipment>> GetAllCancelledShipmentsByIdAsync(int id);
        public Task<IEnumerable<Shipment>> GetAllCancelledShipmentsAsync();
        public Task<Shipment> GetSpecificShipmentAsync(int id);
        public Task<IEnumerable<Shipment>> GetAllShipmentsByCustomer(int id);
        public Task<Shipment> AddShipmentAsync(Shipment shipment);
        public Task UpdateShipmentAdminAsync(Shipment shipment);
        public Task UpdateShipmentUserAsync(Shipment shipment);
        public Task DeleteShipmentAsync(int id);
        public bool ShipmentExists(int id);
    }
}
