﻿using Boxinator.Models;
using Boxinator.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services.ShipmentService
{
    public class ShipmentService : IShipmentService
    {
        private readonly BoxinatorDbContext _context;
        public ShipmentService(BoxinatorDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Shipment>> GetAllShipmentsAsync()
        {
            return await _context.Shipments
                .Include(s => s.Account)
                .ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetAllActiveShipmentsAsync()
        {
            return await _context.Shipments
                .Include(s => s.Account)
                .Where(s => s.Status != StatusType.COMPLETED && s.Status != StatusType.CANCELLED)
                .ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetAllCompletedShipmentsByIdAsync(int id)
        {
            return await _context.Shipments
                .Where(s => s.AccountId == id)
                .Where(s => s.Status == StatusType.COMPLETED)
                .ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetAllCompletedShipmentsAsync()
        {
            return await _context.Shipments
                .Include(s => s.Account)
                .Where(s => s.Status == StatusType.COMPLETED)
                .ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetAllNonCompletedShipmentsAsync(int id)
        {
            return await _context.Shipments
                .Where(s => s.AccountId == id)
                .Where(s => s.Status != StatusType.COMPLETED)
                .ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetAllCancelledShipmentsByIdAsync(int id)
        {
            return await _context.Shipments
                .Where(s => s.AccountId == id)
                .Where(s => s.Status == StatusType.CANCELLED)
                .ToListAsync();
        }

        public async Task<IEnumerable<Shipment>> GetAllCancelledShipmentsAsync()
        {
            return await _context.Shipments
                .Include(s => s.Account)
                .Where(s => s.Status == StatusType.CANCELLED)
                .ToListAsync();
        }

        public async Task<Shipment> GetSpecificShipmentAsync(int id)
        {
            return await _context.Shipments
                .Include(s => s.Account)
                .Where(s => s.Id == id)
                .FirstAsync();
        }

        public async Task<IEnumerable<Shipment>> GetAllShipmentsByCustomer(int id)
        {
            return await _context.Shipments
                .Include(s => s.Account)
                .Where(s => s.AccountId == id)
                .ToListAsync();
        }

        public async Task<Shipment> AddShipmentAsync(Shipment shipment)
        {
            _context.Shipments.Add(shipment);
            _context.Entry(shipment)
                .Property(a => a.Date).IsModified = false;
            await _context.SaveChangesAsync();
            return shipment;
        }

        public async Task UpdateShipmentAdminAsync(Shipment shipment)
        {
            _context.Entry(shipment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateShipmentUserAsync(Shipment shipment)
        {
            shipment.Status = StatusType.CANCELLED;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteShipmentAsync(int id)
        {
            var shipment = await _context.Shipments.FindAsync(id);
            _context.Shipments.Remove(shipment);
            await _context.SaveChangesAsync();
        }

        public bool ShipmentExists(int id)
        {
            return _context.Shipments.Any(a => a.Id == id);
        }
    }
}
