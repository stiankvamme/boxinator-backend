﻿using Boxinator.Models;
using Boxinator.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services.TwoFactorService
{
    public class TwoFactorService : ITwoFactorService
    {
        private readonly BoxinatorDbContext _context;
        public TwoFactorService(BoxinatorDbContext context)
        {
            _context = context;
        }

        public async Task<TwoFactor> GetTwoFactorAsync(int id)
        {
            return await _context.TwoFactor
                .Where(t => t.UserId == id)
                .FirstAsync();
        }

        public async Task<TwoFactor> AddTwoFactorAsync(TwoFactor twoFactor)
        {
            _context.TwoFactor.Add(twoFactor);
            await _context.SaveChangesAsync();
            return twoFactor;
        }

        public async Task DeleteTwoFactorAsync(int id)
        {
            var twoFactor = await _context.TwoFactor.FindAsync(id);
            _context.TwoFactor.Remove(twoFactor);
            await _context.SaveChangesAsync();
        }

        public bool TwoFactorCodeExists(int userId)
        {
            return _context.TwoFactor.Any(e => e.UserId == userId);
        }
    }
}
