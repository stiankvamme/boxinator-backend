# The Boxinator

## Description
A RESTful Web API written with ASP.NET Core and Entity Framework Code First. The API have been made in order to be hosted on Azure and connected with a frontend framework written in Angular and hosted on Heroku.
View the Supplemented User Manual for more information about intended usage, or look to the API documentation at the end of this README.

## Installation
Install AutoMapper.Extensions.Microsoft.DependencyInjection

``````bash
dotnet add [PROJECT NAME] package AutoMapper.Extensions.Microsoft.DependencyInjection --version 8.1.1 
``````

Install Google.Apis.Auth

``````bask
dotnet add [PROJECT NAME] package Google.Apis.Auth --version 1.55.0
``````

Install HtmlSanitizer

``````bash
dotnet add [Project Name] package HtmlSanitizer --version 6.0.441
``````

Install Microsoft.AspNetCore.Cors

``````bash
dotnet add [Project Name] package Microsoft.AspNetCore.Cors --version 2.2.0
``````

Install Microsoft.EntityFrameworkCore

``````bash
dotnet add [Project Name] package Microsoft.EntityFrameworkCore --version 5.0.10
``````

Install Microsoft.EntityFrameworkCore.SqlServer

``````bash
dotnet add [Project Name] package Microsoft.EntityFrameworkCore.SqlServer --version 5.0.10
``````

Install Microsoft.EntityFrameworkCore.Tools

``````bash
dotnet add [Project Name] package Microsoft.EntityFrameworkCore.Tools --version 5.0.10
``````

Install NETCore.MailKit

``````bash
dotnet add [Project Name] package NETCore.MailKit --version 2.0.3
``````

Install SendGrid

``````bash
dotnet add [Project Name] package SendGrid --version 9.24.4
``````

Install Swashbuckle.AspNetCore

``````bash
dotnet add [Project Name] package Swashbuckle.AspNetCore --version 6.2.2
``````

## Usage
For information about usage, look at the supplemented User Manual:
- https://gitlab.com/benjaminandersen/boxinator/-/blob/master/user_manual_the_boxinator.pdf

The Web API (Swagger) hosted on Azure can be viewed on:
- https://boxinatorshipments.azurewebsites.net/swagger/index.html
	- The Azure subscription will be ended 1 month after deployment due to cost related issues.

The full application can be visited on Heroku:
- https://boxinator-shipments.herokuapp.com
	- This site will also be affected when the Azure subscription ends.

Following comes a description on how to run the API locally.

### Before running the application
Navigate to appsettings.json and 'DefaultConnection': switch out 'INSERT SERVER NAME' with your specific server name.

#### If also running together with the frontend of this application:
Navigate to the ShipmentsController.cs, find the Task 'CreateAndSendMailAsync' and comment in the line with 'string Filepath' and comment out the production line.

Navigate to the SendVerificationCodeMail.cs, find the Task 'SendCodeMail' and comment in and out the same as on the line above.

Finally, navigate to the Mailtemplate folder, and go to receipt.html. On line 887 and 888, replace href link to Heroku with the localhost port you are running the frontend on.

### Building Database
Navigate to Package Manager Console:

1. Type 'add-migration' followed by migration name in console to initiate migrations

``````bash
add-migration 'migrationName'
``````

2. Type 'update-database' to run migrations

``````bash
update-database
``````

### Run Application
If the application is being run locally without frontend (Swagger only), then '[GoogleAuthorize]' needs to be removed from the API requests in the various Controllers.

Run API application by pressing IIS Express where the green arrow is displayed.

Note: If you get a CORS related error while trying to run the full application on localhost (frontend and backend), try switching to another browser like Chrome or Edge.

Note: If you is setting up this project on your workspace, it is recommended to create your own SendGrid account and generate your own ApiKey. This should replace the current 'MailKey' which is located under 'MailValues' in appsettings.json. Navigate to SendGrid with the provided link below:
- https://sendgrid.com

## API Documentation

Following comes a guide that documents the Web API in detail.

#### Accounts
	1. Get All Accounts (GET)
		- Get all accounts in database.
		- Possible Error Message:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user accessing accounts is not admin.

	2. Get Account by ID (GET)
		- Get a specific account by ID.
		- Takes in ID of the requested user, and the ID of user performing the action to verify administrator role (if IDs do not match)
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If ID of the requested user and the ID of the user performing the action does not match and user performing the actions is not an admin (Registered users can only view their own account).
			- 404 Not Found - If user is not found in the database.

	3. Update Account (PUT)
		- Update a specific account by ID.
		- Note: account type can not be modified.
		- Takes in ID of the requested user, and user object with changes to be registered.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 400 Bad Request - If user ID and ID of the account object does not match, or if Firstname, Lastname, or Email is empty.
			- 404 Not Found - If account does not exist in the database.

	4. Make Admin (PUT)
		- Update a specific user's AccountType to become Administrator by ID
		- Takes in the ID of the user that is going to be elevated to admin, and ID of user performing the action to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user performing the action is not admin.
			- 404 Not Found - If account (to become administrator) is not found.

	5. Remove Admin (PUT)
		- Update a specific user's AccountType, to change from Administrator to Registered User, by ID
		- Takes in the ID of the user that is going to be delegated to Registered User, and ID of user performing the action to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user performing the action is not admin.
			- 404 Not Found - If account (to become administrator) is not found.

	6. Register User (POST)
		- Add a new account to the database.
		- Takes in an Account object.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 409 Conflict - If Email already exists in the database.
			- 400 Bad Request - If Firstname, Lastname, or Email is empty.

	7. Add Guest (POST)
		- Add a new guest user to the database.
		- Note: Only email is stored when creating a shipment (for receipt purposes).
		- Takes in an Account object to read the supplemented email.
		- Possible Error Messages:
			- None (Either email exist and that account is returned, or new email is registered to database).

	8. Google Auth (POST)
		- Authorize user with Google
		- Checks if user already exists or is a new user
		- Takes in an Account object.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.

	9. Delete Account (DELETE)
		- Delete an account by specific ID
		- Takes in the ID of the account that is going to be deleted, and ID of user performing the action to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user that is deleting an account is not an administrator.
			- 404 Not Found - If the user that is being deleted is not found in the database.

#### Countries

	1. Get All Countries (GET)
		- GET request to get all countries in database.

	2. Add Country (POST)
		- Adds a new Country with CountryMiltiplier to the database.
		- Takes in a Country object, and ID of user performing the action to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user adding a country is not admin.
			- 409 Conflict - If Country name already exists.
			- 400 Bad Request - If Country name is empty, or if Country Multiplier is 0 or less.

	3. Update Country (PUT)
		- Updates a Country.
		- Takes in a Country object, ID on the Country to be edited, and ID of user performing the action to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user updating a country is not admin.
			- 400 Bad Request - If provided Country ID does not match the Country objects ID, or if Country Multiplier is 0 or less.
			- 404 Not Found - If Country is not found ind the database.

#### Shipments
	1. Get All Shipments (GET)
		- Get all shipments in database.
		- Takes in ID of the user that is trying to see all shipments to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user getting all shipments is not admin.

	2. Get Active Shipments (GET)
		- Get all non-cancelled and non-completed shipments.
		- Takes in ID of the user that is trying to see all active shipments to verify administrator role.
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user getting active shipments is not admin.

	3. Get Completed Shipments for a user (GET)
		- Get all completed shipments for a specific user in database.
		- Takes in ID of the user whos shipments is being accessed, and ID of user performing the action to verify administrator role (registered users can only access their own shipments).
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not an admin, or not an admin and trying to access shipments with another user's ID.
			- 404 Not Found - If the account whose shipments one is trying to access is not found in the database.

	4. Get All Completed Shipments (GET)
		- Get all completed shipments in database.
		- Takes in ID of the user that is trying to see all completed shipments to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not an admin.

	5. Get All Non-Completed Shipments (GET)
		- Get all non-completed shipments for a specific user in database.
		- Takes in the ID of the account that is going to viewed, and ID of user performing the action to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not an admin, and trying to view someone else's account (IDs do not match).
			- 404 Not Found - If the Account whose shiptments one is trying to access is not found in the database.

	6. Get Cancelled Shipments for a user (GET)
		- Get all cancelled shipments for a specific user in database.
		- Takes in ID of the user who's shipments is being accessed, and ID of user performing the action to verify administrator role (registered users can only access their own shipments).
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not an admin, or not and admin and trying to access shipments with another user's ID.
			- 404 Not Found - If the account whose shipments one is trying to access is not found in the database.

	7. Get All Cancelled Shipments (GET)
		- Get all cancelled shipments in database.
		- Takes in ID of the user that is trying to see all cancelled shipments to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not an admin.

	8. Get Shipment (GET)
		- Get a specific shipment by ID.
		- Takes in ID of the user who's shipment is being accessed, and ID of user performing the action to verify administrator role (registered users can only access their own shipments).
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 404 Not Found - The specific shipment is not found in the database.
			- 401 Unauthorized - If user is not an admin and trying to access a shipment with an other user's accountID.

	9. Get All Shipments for a user (GET)
		- Get all shipments for a specific customer by account ID.
		- Takes in ID of the user who's shipments is being accessed, and ID of user performing the action to verify administrator role (registered users can only access their own shipments).
		- Possible Error Messages: 
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not admin, then ID must be equal to user ID, so regular users only can access their own shipments.
			- 404 Not Found - The specific user account is not found in the database.

	10. Create Shipment (POST)
		- Create new shipment, and send mail with reciept.
		- Takes in a shipment object with the new shipment that is to be created.
		- Possible Error Messages:
			- 400 Bad Request:
				- If ReceiverName is empty.
				- If Status is not StatusType.CREATED.
				- If shipment Weight is not equal to 1, 2, 5, or 8 (kg).
				- If DestinationCountry does not exist in the database.
				- If the shipping price is not correct.
	
	11. Create and Send Mail
		- Creates a mail and mail template from html and adds values to shippment data - by reading html file to string, before replacing in that string.
		- Takes in mail (string) and a shipment object.
		- Possible Error Messages:
			- None.

	12. Update a Shipment (PUT)
		- Update shipment by ID, only accessible for admins.
		- Takes in the ID of the shipment that is being updated, a shipment object with the new changes, and ID of user performing the action to verify adminstrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not admin.
			- 404 Not Found - If shipment is not found in the database.
			- 400 Bad Request:
				- If receiver name is empty.
				- If shipment status is invalid.
				- If shipment weight is not valid (1, 2, 5, or 8).
				- If destination country does not exist in the database.
				- If the shipping price is not calculated correctly.
				- If ID on the shipment that is being updated is not the same as shipmentId from the updated shipment object.

	13. Cancel Shipment (PUT)
		- Change shipment status to CANCELLED.
			- Check if userId same as accountId on shipment.
			- Check if status is valid (CANCELLED).
		- Takes in a shipment object and user ID of the user who is performing the action.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 404 Not Found - If the shipment is not found in the database.
			- 401 Unauthorized - If shipmentId is not the same as user ID

	14. Delete Shipment (DELETE)
		- Delete a shipment by ID.
		- Takes in the ID of the shipment that is being deleted, and ID of user performing the action to verify administrator role.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 401 Unauthorized - If user is not an administrator.
			- 404 Not Found - If the shipment that is being deleted can not be found in the database.


#### TwoFactor
	1. Get TwoFactor (GET)
		- Get a TwoFactor code from the database by ID.
		- Takes in ID of the TwoFactor code that is being accessed.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.

	2. Check Verification Code (POST)
		- Check if the verification code that the user has entered is the same as in database, and within six minutes of when it was registered.
		- Deletes the verification code from the database after validation if correct.
		- Takes in a TwoFactor object.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.

	3. Add Verification Code (POST)
		- Add verification code to database (with timestamp).
		- Takes in a TwoFactor object.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.

	4. Delete TwoFactor (DELETE)
		- Delete verification code by ID
		- Takes in ID of the TwoFactor code that is being deleted.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.

	5. New TwoFactor Code (POST)
		- Get new TwoFactor code.
		- Pressed if user need a new code after unsuccessfully trying to validate with the first.
		- Returns '200 OK' if successfull.
		- Possible Error Messages:
			- 500 Internal Server Error - If user is not Authorized through Google.
			- 400 Bad Request - If user ID is null.
